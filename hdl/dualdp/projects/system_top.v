// ***************************************************************************
// ***************************************************************************
// Copyright 2011(c) Analog Devices, Inc.
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//     - Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     - Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the
//       distribution.
//     - Neither the name of Analog Devices, Inc. nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//     - The use of this software may or may not infringe the patent rights
//       of one or more patent holders.  This license does not release you
//       from the requirement that you obtain separate licenses from these
//       patent holders to use this software.
//     - Use of the software either in source or binary form, must be run
//       on or directly connected to an Analog Devices Inc. component.
//
// THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED.
//
// IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, INTELLECTUAL PROPERTY
// RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ***************************************************************************
// ***************************************************************************

`timescale 1ns/100ps

module system_top (

//  input           clk_out,
  input           clk_in_p,
  input           clk_in_n,
  input           clk_in2_p,
  input           clk_in2_n,
  input           dp_aux_din,
  output          dp_aux_dout,
  output          dp_aux_oe,
  input           dp_hpd,
  input   [12:0]  gpio_bd_i,
  output  [ 7:0]  gpio_bd_o,
  inout   [19:0]  gpios,
  input           pps_p,
  input           pps_n,
  input           rx_clk_in_p,
  input           rx_clk_in_n,
  input           rx_frame_in_p,
  input           rx_frame_in_n,
  input   [ 5:0]  rx_data_in_p,
  input   [ 5:0]  rx_data_in_n,
  output          tx_clk_out_p,
  output          tx_clk_out_n,
  output          tx_frame_out_p,
  output          tx_frame_out_n,
  output  [ 5:0]  tx_data_out_p,
  output  [ 5:0]  tx_data_out_n,

  output          enable,
  output          txnrx,

  output          gpio_resetb,
  output          gpio_sync,
  output          gpio_en_agc,
  output  [ 3:0]  gpio_ctl,
  input   [ 7:0]  gpio_status,
  input           fan_tach);

  // internal signals
  localparam TCQ = 1;

  wire    [95:0]  gpio_i;
  wire    [95:0]  gpio_o;
  wire            usr_clk;
  wire            rst_n;
  wire            dp_aux_oe_n;
  wire            clk_in;
  wire            clk;
  wire            clk_in2;
  wire            clk2;
  wire            pps;

// defaults

  assign gpio_resetb = gpio_o[46:46];
  assign gpio_sync = gpio_o[45:45];
  assign gpio_en_agc = gpio_o[44:44];
  assign gpio_ctl = gpio_o[43:40];
  assign gpio_bd_o = gpio_o[20:13];
  
  IBUFDS i_pps (
      .O  (pps),
      .I  (pps_p),
      .IB (pps_n)
    );
  IBUFDS i_ibufds2 (
      .O  (clk_in2),
      .I  (clk_in2_p),
      .IB (clk_in2_n)
    );

  BUFG i_bufg2(
      .O (clk2),
      .I (clk_in2)
    );
  
  IBUFDS i_ibufds (
    .O  (clk_in),
    .I  (clk_in_p),
    .IB (clk_in_n)
  );
  
  BUFG i_bufg(
    .O (clk),
    .I (clk_in)
  );
  //assign gpio_i[95:40] = gpio_o[95:40];
  //assign gpio_i[39:32] = gpio_status;
  //assign gpio_i[31:13] = gpio_o[31:13];
  //assign gpio_i[12: 0] = gpio_bd_i;
  assign gpio_i[0] = pps;

  // instantiations
  fan_ctrl i_fan_ctrl (
    .clk (clk),
    .rst (!rst_n),
//    .PAD_FAN_PWM (fan_pwm),
    .PAD_FAN_TACH (fan_tach),
    .fan_status_out (fan_status),
    .fan_control_in (fan_control)
  );
  
  assign dp_aux_oe = ~dp_aux_oe_n;
  system_wrapper i_system_wrapper (
    .dp_aux_din(dp_aux_din),
    .dp_aux_dout(dp_aux_dout),
    .dp_aux_oe(dp_aux_oe_n),
    .dp_hpd(dp_hpd),
    .enable (enable),
	.fan_control_tri_o (fan_control),
    .fan_status_tri_i (fan_status),

    .gpio_i (gpio_i),
    .gpio_o (gpio_o),
    .GPIO_tri_io(gpios),
    .pl_clk0(usr_clk),
    .pl_rst_n(rst_n),

    .ps_intr_00 (1'b0),
    .ps_intr_01 (1'b0),
    .ps_intr_02 (1'b0),
    .ps_intr_03 (1'b0),
    .ps_intr_04 (1'b0),
    .ps_intr_05 (1'b0),
    .ps_intr_06 (1'b0),
    .ps_intr_07 (1'b0),
    .ps_intr_08 (1'b0),
    .ps_intr_09 (1'b0),
    .ps_intr_10 (1'b0),
    .ps_intr_11 (1'b0),
    .ps_intr_14 (1'b0),
    .ps_intr_15 (1'b0),
    .rx_clk_in_n (rx_clk_in_n),
    .rx_clk_in_p (rx_clk_in_p),
    .rx_data_in_n (rx_data_in_n),
    .rx_data_in_p (rx_data_in_p),
    .rx_frame_in_n (rx_frame_in_n),
    .rx_frame_in_p (rx_frame_in_p),
    .tdd_sync_i (1'b0),
    .tdd_sync_o (),
    .tdd_sync_t (),
    .tx_clk_out_n (tx_clk_out_n),
    .tx_clk_out_p (tx_clk_out_p),
    .tx_data_out_n (tx_data_out_n),
    .tx_data_out_p (tx_data_out_p),
    .tx_frame_out_n (tx_frame_out_n),
    .tx_frame_out_p (tx_frame_out_p),
    .txnrx (txnrx),
    .up_enable (gpio_o[47]),
    .up_txnrx (gpio_o[48]));

endmodule

// ***************************************************************************
// ***************************************************************************
