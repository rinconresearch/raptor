Raptor is the next-generation software-defined radio platform from Rincon
Research. Raptor combines state-of-the-art capabilities with a flexible
design, resulting in a compact, efficient solution for multiple mission
requirements.

* [Product Information](http://www.rincon.com/products/board-level/raptor/)
* [User Guide](http://raptor.rincon.com/)
* [Issue Tracker](https://gitlab.com/rinconresearch/raptor/issues)

See the wiki for instructions on creating a bootable SD card image.


## Libiio and IIO Daemon


The Raptor BSP comes with the IIO Daemon (IIOD) server that enables you to
transmit and receive RF signals out of the box. Following is the list of
network backends that can be used to stream data to.

1. [MATLAB and Simulink](https://wiki.analog.com/resources/tools-software/linux-software/libiio/clients/matlab_simulink)
2. [Visual Analog](https://wiki.analog.com/resources/tools-software/linux-software/libiio/clients/visual_analog)
3. [IIO Oscilloscope](https://wiki.analog.com/resources/tools-software/linux-software/iio_oscilloscope)
4. [GNU Radio](https://wiki.analog.com/resources/tools-software/linux-software/gnuradio)
5. [GNU Radio Raptor Blocks](https://gitlab.com/rinconresearch/gr-raptor/)

## Source Code and Reference Designs

Analog Devices provide a set of drivers, libraries and reference designs for the AD9361 part. The Raptor SDR includes them in the BSP that is included with the board.

### Analog Devices AD9361 RF Transceiver

Analog Devices provides complete drivers for the AD9361 for both bare metal/No-OS and operating systems (Linux). The AD9361 drivers can be found at:

[Linux](https://wiki.analog.com/resources/tools-software/linux-drivers/iio-transceiver/ad9361?doc=AD9361_Reference_Manual_UG-570.pdf) - wiki page and device driver customization.

[No-OS](https://wiki.analog.com/resources/eval/user-guides/ad-fmcomms2-ebz/software/baremetal?doc=AD9361_Reference_Manual_UG-570.pdf) - wiki page.

[FPGA](https://wiki.analog.com/resources/eval/user-guides/ad-fmcomms2-ebz/reference_hdl) - HDL Reference Design.
